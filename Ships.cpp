/*
+----------------------------------------------------------+
This game is called Ships. 
Play it single or with your friends. It is up to you.
Two gameplay modes available: Singleplayer and Competition

author: Jindrich Simek
+----------------------------------------------------------+
*/

#include "Ships.hpp"

int gameMode, choice = 0;

//All information about ships for small variant (8x8)
vector<Ship> smallVariant 
{
    {"Boat", 1, 4, 'B'},
    {"Support Boat", 2, 3, 'S'},
    {"Battle Ship", 3, 2, 'A'},
    {"Aircraft Carrier", 4, 1, 'C'}
};

//All information about ships for big variant (10x10)
vector<Ship> bigVariant 
{
    {"Boat", 1, 5, 'B'},
    {"Support Boat", 2, 3, 'S'},
    {"Battle Ship", 3, 3, 'A'},
    {"Aircraft Carrier", 4, 2, 'C'}
};


Game::Game(int size, vector<Ship> shipTypes, bool isSinglePlayer, bool isCompetition, string player1, string player2, int space)
    
    : gridSize(size), ships(shipTypes), singlePlayer(isSinglePlayer), competitionMode(isCompetition), player1Name(player1), player2Name(player2), spacing(space) 
    {
    
    //sizes first player's tables/grids according to chosen gridsize and fills them with characters
    grid1.resize(gridSize, vector<char>(gridSize, '~'));
    displayGrid1.resize(gridSize, vector<char>(gridSize, '?'));

    //sizes second player's tables/grids according to chosen gridsize and fills them with characters
    grid2.resize(gridSize, vector<char>(gridSize, '~'));
    displayGrid2.resize(gridSize, vector<char>(gridSize, '?'));

    //anullates all ship hits
    for(const auto& ship : ships) 
    {
        shipHitCount1[ship.symbol] = 0;
        shipHitCount2[ship.symbol] = 0;
    }

    //places ships in first table according to random seed
    placeShips(grid1, time(0));

    if(!singlePlayer) 
    {
        //places ships in second grid according to random seed
        placeShips(grid2, time(0) + 1); 
    }
}

//places ships accroding to random seed
void Game::placeShips(vector<vector<char>>& grid, unsigned int seed) 
{
    srand(seed);
    for(const auto& ship : ships) 
    {
        for(int i = 0; i < ship.count; ++i) 
        {
            bool placed = false;
            while(!placed) 
            {
                int x = rand() % gridSize;
                int y = rand() % gridSize;

                bool horizontal = rand() % 2;
                if(canPlaceShip(x, y, ship.size, horizontal, grid)) 
                {
                    for(int j = 0; j < ship.size; ++j) 
                    {
                        if(horizontal)
                        {
                            grid[x][y + j] = ship.symbol;
                        }
                        else
                        { 
                            grid[x + j][y] = ship.symbol;
                        }
                    }
                    placed = true;
                }
            }
        }
    }
}

//checks if it is possible to place ship
bool Game::canPlaceShip(int x, int y, int size, bool horizontal, vector<vector<char>>& grid) 
{
    if (horizontal)
    {
        if(y + size > gridSize) return false;
        for(int i = 0; i < size; ++i) if(grid[x][y + i] != '~') return false;
    }
    else
    {
        if(x + size > gridSize) return false;
        for(int i = 0; i < size; ++i) if(grid[x + i][y] != '~') return false;
    }
    return true;
}

// displays competition mode
void Game::display(const vector<vector<char>>& displayGrid1, const vector<vector<char>>& displayGrid2, const string& playerName1, const string& playerName2)
{
    
    //ensures minimum spacing between two tables/grids is 30
    string space(max(spacing, 30), ' ');

    //writes players table marking according to grid size
    if(choice == 1) 
    {
        cout << " " << playerName1 << "'s field" << string(spacing - playerName1.size() - 6 + 34, ' ') << playerName2 << "'s field" << endl;
    }
    else
    {
        cout << " " << playerName1 << "'s field" << string(spacing - playerName1.size() - 6 + 42, ' ') << playerName2 << "'s field" << endl;
    }

    //writes markings for each cell
    cout << "    ";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << setw(2) << char('A' + i) << "  ";
    }
    cout << space << "    ";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << setw(2) << char('A' + i) << "  ";
    }

    //displays first table line
    cout << endl << "   +";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << "---+";
    }
    cout << space << "   +";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << "---+";
    }
    cout << endl;

    //displays rest of the tables
    for(int i = 0; i < gridSize; ++i)
    {
        cout << setw(2) << i + 1 << " |";
        for(int j = 0; j < gridSize; ++j)
        {
            cout << " " << displayGrid1[i][j] << " |";
        }
        cout << space << setw(2) << i + 1 << " |";
        for(int j = 0; j < gridSize; ++j)
        {
            cout << " " << displayGrid2[i][j] << " |";
        }
        cout << endl << "   +";
        for(int k = 0; k < gridSize; ++k)
        {
            cout << "---+";
        }
        cout << space << "   +";
        for(int k = 0; k < gridSize; ++k)
        {    
            cout << "---+";
        }
        cout << endl;
    }
}

//displayes singleplayer mode
void Game::displaySingle(const vector<vector<char>>& displayGrid)
{
    
    cout << "    ";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << setw(2) << char('A' + i) << "  ";
    }
    cout << endl << "   +";
    for(int i = 0; i < gridSize; ++i)
    {
        cout << "---+";
    }
    cout << endl;
    for(int i = 0; i < gridSize; ++i)
    {
        cout << setw(2) << i + 1 << " |";
        for(int j = 0; j < gridSize; ++j)
        {
            cout << " " << displayGrid[i][j] << " |";
        }
        cout << endl << "   +";
        for(int k = 0; k < gridSize; ++k)
        {
            cout << "---+";
        }
        cout << endl;
    }
}

//Chooses with player is on turn, returns true or false
bool Game::playTurn(vector<vector<char>>& grid, vector<vector<char>>& displayGrid, map<char, int>& shipHitCount, string playerName, bool isOpponentTurn)
{
    int x, y;
    bool validMove = false;

    while(!validMove)
    {
        if((competitionMode) && (isOpponentTurn))
        {
            display(displayGrid1, displayGrid2, player1Name, player2Name);
        }
        else
        {
            displaySingle(displayGrid);
        }

        cout << playerName << ", enter coordinates to hit (letter number, A1): ";
        string input;
        cin >> input;

        if((input.length() < 2) || (input.length() > 3))
        {
            cout << "Invalid input format. Try again." << endl;
            continue;
        }

        char cx = toupper(input[0]);
        stringstream ss(input.substr(1));
        if(!(ss >> y))
        {
            cout << "Invalid input format. Try again." << endl;
            continue;
        }

        x = cx - 'A';
        y -= 1;

        if(x < 0 || x >= gridSize || y < 0 || y >= gridSize)
        {
            cout << "Invalid coordinates. Try again." << endl;
            continue;
        }

        if((displayGrid[y][x] == 'X') || (displayGrid[y][x] == '0'))
        {
            cout << "You already hit this spot. Try again.";
            if(gameMode == 2)
            {
                if(choice == 1)
                {
                    cout << string(spacing - 1, ' ') << "You already hit this spot. Try again." << endl;
                }
                else
                {
                    cout << string(spacing + 7, ' ') << "You already hit this spot. Try again." << endl;
                }
            }
            else
            {
                cout << endl;
            }
            validMove = true;
        }
        else if(grid[y][x] != '~' && displayGrid[y][x] != 'X')
        {
            displayGrid[y][x] = 'X';
            char shipSymbol = grid[y][x];
            shipHitCount[shipSymbol]++;

            cout << "Hit! You hit a " << getShipName(shipSymbol) << "!" << endl;
            if(shipHitCount[shipSymbol] == getShipSize(shipSymbol))
            {
                cout << "You sunk a " << getShipName(shipSymbol) << "!" << endl;
            }
            validMove = true;
        }
        else
        {
            displayGrid[y][x] = '0';
            if(gameMode == 1)
            {
                cout << "Miss!" << endl;
            }
            else
            {
                if(choice == 1)
                {
                    cout << "Miss!" << string(spacing + 31, ' ') << "Miss!" << endl;
                }
                else
                {
                    cout << "Miss!" << string(spacing + 39, ' ') << "Miss!" << endl;
                }
            }
            validMove = true;
        }
    }
    return checkWin(shipHitCount);
}

//plays singleplayer mode
void Game::playSinglePlayer()
{
    string defaultPlayerName = "Player";
    while(true)
    {
        if (playTurn(grid1, displayGrid1, shipHitCount1, defaultPlayerName))
        {
            cout << "Congratulations, " << defaultPlayerName << "! You have sunk all the ships!" << endl;
            break;
        }
    }
}

//plays competition mode
void Game::playCompetition(const string& playerName1, const string& playerName2)
{
    bool player1Turn = true;
    while(true)
    {
        if(player1Turn)
        {
            cout << endl << player2Name << "'s turn";
            if(choice == 1)
            {
                cout << string(spacing - playerName2.size() - 6 + 35, ' ');
            }
            else
            {
                cout << string(spacing - playerName2.size() - 6 + 43, ' ');
            }
            cout << "It's your turn" << endl;

            if(playTurn(grid1, displayGrid1, shipHitCount1, player2Name, true))
            {
                cout << player2Name << " wins! All " << player1Name << "'s ships have been sunk!" << endl;
                break;
            }
        }
        else
        {
            cout << endl << "It's your turn";
            if(choice == 1)
            {
                cout << string(spacing + 22, ' ');
            }
            else
            {
                cout << string(spacing + 30, ' ');
            }
            cout << player1Name << "'s turn" << endl;
            if(playTurn(grid2, displayGrid2, shipHitCount2, player1Name, true))
            {
                cout << player1Name << " wins! All " << player2Name << "'s ships have been sunk!" << endl;
                break;
            }
        }
        player1Turn = !player1Turn;
    }
}

//gets name of the ship
string Game::getShipName(char symbol)
{
    for(const auto& ship : ships)
    {
        if(ship.symbol == symbol)
        {
            return ship.name;
        }
    }
    return "Unknown Ship";
}

//gets ship size
int Game::getShipSize(char symbol)
{
    for(const auto& ship : ships)
    {
        if(ship.symbol == symbol)
        {
            return ship.size;
        }
    }
    return 0;
}

//checks who won the game
bool Game::checkWin(const map<char, int>& shipHitCount)
{
    for(const auto& ship : ships)
    {
        if(shipHitCount.at(ship.symbol) < ship.size * ship.count)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    while(true)
    {
        cout << "Choose game mode: 1 for Single Player, 2 for Competition: ";
        cin >> gameMode;
        if(gameMode == 1 || gameMode == 2)
        {
            break;
        }
        cout << "Invalid choice. Please enter 1 or 2." << endl;
    }
    while(true)
    {
        cout << "Choose game variant: 1 for small (8x8), 2 for big (10x10): ";
        cin >> choice;
        if(choice == 1 || choice == 2)
        {
            break;
        }
        cout << "Invalid choice. Please enter 1 or 2." << endl;
    }

    bool isSinglePlayer = (gameMode == 1);
    bool isCompetition = (gameMode == 2);
    int gridSize = (choice == 1) ? 8 : 10;
    vector<Ship> ships = (choice == 1) ? smallVariant : bigVariant;

    string player1Name = "Player", player2Name;
    int spacing = 30;

    if(isCompetition)
    {
        cout << "Player 1 enter your nickname (max 20 characters): ";
        cin.ignore();
        getline(cin, player1Name);
        if(player1Name.length() > 20)
        {
            player1Name = player1Name.substr(0, 20);
            cout << "Name shortened to: " << player1Name << endl;
        }

        cout << "Player 2 enter your nickname (max 20 characters): ";
        getline(cin, player2Name);
        if(player2Name.length() > 20)
        {
            player2Name = player2Name.substr(0, 20);
            cout << "Name shortened to: " << player2Name << endl;
        }

        while(true)
        {
            cout << "Enter the spacing between the two grids (minimum 30): ";
            cin >> spacing;
            if(spacing >= 30)
            {
                break;
            }
            cout << "Invalid spacing. Please enter a number greater than or equal to 30." << endl;
        }
    }

    Game game(gridSize, ships, isSinglePlayer, isCompetition, player1Name, player2Name, spacing);

    if(isSinglePlayer)
    {
        game.playSinglePlayer();
    }
    else
    {
        game.playCompetition(player1Name, player2Name);
    }

    return 0;
}
