/*
+---------------------------------------------------------------------------------+
This is .hpp file holds all own functions, vectors and classes needed for Ships.cpp
+---------------------------------------------------------------------------------+
*/
#ifndef SHIPS_HPP
#define SHIPS_HPP

#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iomanip>
#include <map>
#include <string>
#include <sstream>

using namespace std;

extern int gameMode;
extern int choice;

//This structure hold all importatnt information about ships
struct Ship {
    string name;
    int size;
    int count;
    char symbol;
};

//These vectors hold all ship for each variant of the game
extern vector<Ship> smallVariant;
extern vector<Ship> bigVariant;

class Game {

	//Stores table of each player
    vector<vector<char>> grid1, grid2;

	//Stores table of each player to be displayed
    vector<vector<char>> displayGrid1, displayGrid2;

	//All information about ship
    vector<Ship> ships;

	//how many times was each ship hit 
    map<char, int> shipHitCount1, shipHitCount2;

    int gridSize;
    bool singlePlayer, competitionMode;

	//players name
    string player1Name, player2Name;

	//spacing between two tables
    int spacing;

public:
	//contructor
    Game(int size, vector<Ship> shipTypes, bool isSinglePlayer, bool isCompetition, string player1, string player2, int space = 30);

	//Places ships on the grid according to random seed
    void placeShips(vector<vector<char>>& grid, unsigned int seed);

	//returns value if it is possible to place ship
    bool canPlaceShip(int x, int y, int size, bool horizontal, vector<vector<char>>& grid);

	//displays game
    void display(const vector<vector<char>>& displayGrid1, const vector<vector<char>>& displayGrid2, const string& playerName1, const string& playerName2);

	//displayes singleplayer mode
    void displaySingle(const vector<vector<char>>& displayGrid);

	//returns value, which player has a turn
    bool playTurn(vector<vector<char>>& grid, vector<vector<char>>& displayGrid, map<char, int>& shipHitCount, string playerName, bool isOpponentTurn = false);

	//plays singleplaye mode
    void playSinglePlayer();

	//plays competition mode
    void playCompetition(const string& playerName1, const string& playerName2);

	//return name of ship
    string getShipName(char symbol);

	//returns value about ship size
    int getShipSize(char symbol);

	//returns value who won the game
    bool checkWin(const map<char, int>& shipHitCount);

	
};

#endif // MAIN_HPP