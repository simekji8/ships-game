# Ships game

## About
Classic easy to use ships game that you can play alone or with your friend.

First of all there are two gamemodes to be played.

1. Singleplayer mode - you are trying to destroy all ships in minimum number of shots

2. Competition mode - you are competing with another player to destroy his ships in fewer hits

You can choose between them by pressing either 1 or 2 on your keyboard and if different number is pressed the program will display error message.

There are also two grid sizes 8x8 and 10x10 (You can choose between them also by pressing 1 or 2), each of these sizes has all 4 ship types (Boat, Support ship, Battle Ship, Aircraft carrier). The game is programmed so you do not have to place ship on your own, but they are placed automatically according to random seed, for both singleplayer and competition mode.

Competition mode is more complex. It requires entering nickname for both players at maximmum lenght of 20. Grid space also must be entered, which can't be lower than 30. It ensures that tables are placed far enough so you can for example move the game window on two screens.

During the game itself player/players are entering coordinates (e.g. A1, F10, ..). When player misses the field is filled with 0. When he hits it is filled with X. 

The game ends when one player destroys all of the opponent's ships or in singleplayer mode all ships are destroyed.

## Step by step guide
 - Run the Ships.exe file

 - Press 1 or 2 to select mode

 - Press 1 or 2 to select grid (8x8 or 10x10)
     -If you selected 1 in first step you are already in singleplayer mode and playing

 - Enter nickname for player 1 (max. 20 characters)

 - Enter nickname for player 2 (max. 20 characters)

 - Enter grid spacing (min. 30)

 - Enjoy the game!

## Compilation

I personally used MinGW, but any other c++ compiler can be used.

You don't have to compile at all. There is Ships.exe file.

## Warning
This version is still full bugs and faults. Please make sure that you are entering EXPECTED parameters, otherwise game may not work as intended.

MOST bugs were already tracked and fixed...

Two are known at the moment:
 - by entering [letter][number][letter] instead of coordinates makes the game think that you are entering coordinates

 - entering letter instead of number in gamemode selection or grid size causes program to repeatedly write to terminal.
